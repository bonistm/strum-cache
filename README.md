# Strum::Cache

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/strum/cache`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'strum-cache'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install strum-cache

## Usage

###ENV variables:

`CACHE_REDIS_URL` - connection string to Redis

`{RESOURCE}_CACHE_REDIS_URL` - separated Redis connection string for resource 

`BASE_RESOURCE_URL` - full url to resource find endpoint. example: "http://example.com/entities"

`{RESOURCE}_RESOURCE_URL` - separated full url to resource find endpoint. example: "http://example.com/entities"

`BASE_RESOURCE_SEARCH_URL` - full url to resource search endpoint. example: "http://example.com/entities/search"

`{RESOURCE}_RESOURCE_SEARCH_URL` - separated full url to resource search endpoint. example: "http://example.com/entities/search"

###Configurable

`Strum::Cache.config.cache_headers` - hash with headers that should be sent on http request

`Strum::Cache::config.redis_class` - class of Redis. Default is `Redis`, can be changed for mock

###How to use

`Strum::Cache::Entity::Find.(id)` - find Entity by id

`Strum::Cache::Entity::Search.(params)` - search Entity by params

`Strum::Cache::Entity::Push.(params)` - push Entity to cache

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/strum-cache. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/strum-cache/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Strum::Cache project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/strum-cache/blob/master/CODE_OF_CONDUCT.md).
