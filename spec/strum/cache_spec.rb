# frozen_string_literal: true

RSpec.describe Strum::Cache do
  it "has a version number" do
    expect(Strum::Cache::VERSION).not_to be nil
  end
end
