# frozen_string_literal: true

require "redis"
require "singleton"
require "strum/cache_utils/redis_storage"

module Strum
  module CacheUtils
    # Redis storage
    class Redis
      include Strum::Service

      def call
        if (entity = redis_connection.hgetall(resource_id)).key?("id")
          redis_connection.expire(resource_id, redis_resource_expire)
          output(entity)
        else
          add_error(:entity, :not_found)
        end
      end

      def audit
        required(:resource_code, :resource_id)
      end

      private

        def redis_connection
          Strum::CacheUtils::RedisStorage.const_get(resource_code.to_s.capitalize, false).instance.redis
        end

        def redis_resource_expire
          ENV.fetch("#{resource_code.to_s.upcase}_CACHE_EXPIRE", ENV.fetch("CACHE_EXPIRE", 60 * 15))
        end
    end
  end
end
