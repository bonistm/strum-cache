# frozen_string_literal: true

require "uri"

module Strum
  module CacheUtils
    # Build url to resource
    class BuildResourcesUrl
      include Strum::Service

      def call
        underscore_resource_code = inflector.underscore(resource_code)
        base_url = ENV.fetch("#{underscore_resource_code.upcase}_RESOURCE_SEARCH_URL") do
          host = ENV.fetch("BASE_RESOURCE_SEARCH_URL")
          File.join(host, inflector.pluralize(underscore_resource_code.gsub(/_/, "-")))
        end
        output(url: base_url, params: params)
      rescue KeyError
        add_error("#{resource_code} SEARCH ENV", :not_found)
      end

      def audit
        required(:resource_code, :params)
        add_error(:params, :hash_required) unless params.is_a?(Hash)
      end

      private

        def inflector
          @inflector ||= Dry::Inflector.new
        end
    end
  end
end
