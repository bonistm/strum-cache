# frozen_string_literal: true

require "strum/cache_utils/build_resources_url"

module Strum
  module CacheUtils
    # Resource url builder
    class BuildResourceUrl
      include Strum::Service

      def call
        output(url: File.join(base_url, resource_id.to_s))
      rescue KeyError
        Strum::CacheUtils::BuildResourcesUrl.call(resource_code: resource_code, params: { id: resource_id }) do |m|
          m.success { |result| output(result) }
          m.failure { |errors| add_errors(errors) }
        end
      end

      def audit
        required(:resource_code, :resource_id)
      end

      private

        def inflector
          @inflector ||= Dry::Inflector.new
        end

        def underscore_resource_code
          inflector.underscore(resource_code)
        end

        def base_url
          ENV.fetch("#{underscore_resource_code.upcase}_RESOURCE_URL") do
            host = ENV.fetch("BASE_RESOURCE_URL")
            File.join(host, inflector.pluralize(underscore_resource_code.gsub(/_/, "-")))
          end
        end
    end
  end
end
