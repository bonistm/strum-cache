# frozen_string_literal: true

require "faraday"
require "json"

module Strum
  module CacheUtils
    # Send request to entity source
    class SendRequest
      include Strum::Service

      def call
        (resp = send_request).success? && output(JSON.parse(resp.body))
      rescue Faraday::Error => e
        add_error(:connection, e)
      rescue JSON::JSONError => e
        add_error(:payload, e)
      end

      def audit
        required :url
      end

      private

        def cache_headers
          Strum::Cache.config.cache_headers.is_a?(Hash) ? Strum::Cache.config.cache_headers : {}
        end

        def conn
          Faraday.new(url: url)
        end

        def send_request
          conn.get do |req|
            req.params = params if input[:params]
            req.headers["Content-Type"] = "application/json"
            req.headers["Accept"] = "application/json"
            cache_headers.each { |k, v| req.headers[k] = v }
          end
        end
    end
  end
end
