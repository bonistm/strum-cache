# frozen_string_literal: true

require "singleton"
require "redis"

module Strum
  module CacheUtils
    # Redis storage
    module RedisStorage
      # rubocop: disable Metrics/MethodLength, Metrics/AbcSize
      def self.const_missing(resource_name)
        const_set(resource_name.to_s.strip.capitalize, Class.new do
          include Singleton
          attr_reader :redis

          define_method :initialize do
            redis_url = ENV.fetch("#{resource_name.upcase}_CACHE_REDIS_URL", ENV.fetch("CACHE_REDIS_URL", nil))
            @redis = if redis_url
                       Strum::Cache.config.redis_class.new(url: redis_url)
                     else
                       Strum::Cache.config.redis_class.new
                     end
          end
        end)
      end
      # rubocop: enable Metrics/MethodLength, Metrics/AbcSize
    end
  end
end
